import React from 'react'
import NavLeft from "./components/navigation/nav-left";
import NavBottom from "./components/navigation/nav-bottom";
import NavTop from "./components/navigation/nav-top";
import Main from "./components/main";

const App = () => {
  return (
      <>
        <NavLeft/>
        <NavBottom/>
        <div className="content">
          <NavTop/>
          <Main/>
        </div>

      </>
  )
}

export default App