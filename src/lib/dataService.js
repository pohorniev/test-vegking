export class DataService {
  static findAll() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let data = require('../data.json')
        let products = data.products
        let buyers = data.buyers

        resolve(data)
      }, 1000)
    })
  }

  static findOne(meetupId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let data = require('../data.json')
        let products = data.products
        let product = products.find(el => el.id === parseInt(id))

        if (product) {
          resolve(product)
        } else {
          reject(new Error('not_found'))
        }
      }, 1000)
    })
  }
}