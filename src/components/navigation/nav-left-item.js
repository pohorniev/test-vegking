import React from 'react'

const NavLeftItem = (props) => {
  return (
      <li className={props.active ? `nav-left-menu__item active` : `nav-left-menu__item`}>
        <a href={props.url}>
          {props.children}
        </a>
      </li>
  )
}
export default NavLeftItem;