import React from 'react'
import NavLeftItem from "./nav-left-item";
import logo from '../../assets/logo.png'
import iconStock from '../../assets/icon-stock.png'
import iconMarket from '../../assets/icon-market.png'
import iconWarehouse from '../../assets/icon-warehouse.png'
import iconSettings from '../../assets/icon-gears.png'

const NavBottom = () => {
  return (
      <div className='nav-mob'>
        <div className='nav-bottom'>
          <ul className="nav-bottom-menu">
            <NavLeftItem url={'#'} active>
              <img src={iconStock} />
            </NavLeftItem>
            <NavLeftItem url={'#'}>
              <img src={iconMarket} />
            </NavLeftItem>
            <NavLeftItem url={'#'}>
              <img src={iconWarehouse} />
            </NavLeftItem>
            <NavLeftItem url={'#'}>
              <img src={iconSettings} />
            </NavLeftItem>
          </ul>
        </div>
      </div>
  )
}

export default NavBottom;