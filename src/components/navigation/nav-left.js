import React from 'react'
import NavLeftItem from "./nav-left-item";
import logo from '../../assets/logo.png'
import Stock from '../../assets/icon-stock.svg'
import Market from '../../assets/icon-market.svg'
import Warehouse from '../../assets/icon-warehouse.svg'
import Settings from '../../assets/icon-gears.svg'

const NavLeft = () => {
  return (
      <div className='sidebar'>
        <div className="logo">
          <img src={logo} alt=""/>
        </div>
        <div className='nav-left'>
          <ul className="nav-left-menu">
            <NavLeftItem url={'#'} active>
              <Stock />
            </NavLeftItem>
            <NavLeftItem url={'#'}>
              <Market />
            </NavLeftItem>
            <NavLeftItem url={'#'}>
              <Warehouse />
            </NavLeftItem>
          </ul>

          <ul className="nav-left-bottom">
            <NavLeftItem>
              <Settings />
            </NavLeftItem>
          </ul>
        </div>
      </div>
  )
}

export default NavLeft;