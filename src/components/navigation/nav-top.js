import React, {useState, useEffect, useRef} from 'react'
import userPhoto from '../../assets/user.png'
import IconNotifications from '../../assets/icon-notification.svg'
import IconChevron from '../../assets/icon-down-chevron.svg'

const NavTop = () => {
  const [openNotif, setOpenNotif] = useState(false)
  const [openProfile, setOpenProfile] = useState(false)

  const wrapperRef = useRef(null);

  useEffect(() => {
    function handleClickOutside(event) {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setOpenNotif(false)
        setOpenProfile(false)
      }
    }

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [wrapperRef]);

  return (
      <div className='nav-top'>
        <div className="nav-top-currency">
          <div className="nav-top-currency__item">
            <IconChevron/>
            <span>EUR/PLN: 4.4057</span>
          </div>
          <div className="nav-top-currency__item">
            <IconChevron/>
            <span>GBP/PLN: 4.8866</span>
          </div>
        </div>
        <div className="nav-top-panel">
          <div className="nav-top-notifications" onClick={() => setOpenNotif(!openNotif)}>
            <IconNotifications/>
            <div className="notifications-count">1</div>
            <div className={openNotif ? "notifications-list open" : "notifications-list"} ref={wrapperRef}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. A asperiores at consequatur cumque, dignissimos eum harum incidunt laboriosam nam nobis...
            </div>
          </div>

          <div className="nav-top-profile" onClick={() => setOpenProfile(!openProfile)} ref={wrapperRef}>
            <div className="nav-top-profile__img">
              <img src={userPhoto} alt=""/>
            </div>
            <span className="nav-top-profile__name">
              Profile Name
            </span>

            <div className={openProfile ? "nav-top-profile__list open" : "nav-top-profile__list"}>
              <a href="#">Settings</a>
              <a href="#">Log out</a>
            </div>
          </div>
        </div>


      </div>
  )
}

export default NavTop;