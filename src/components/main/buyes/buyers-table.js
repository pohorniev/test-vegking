import React, {useEffect, useState} from 'react'
import SortIcons from "./sort-icons";
import {DataService} from "../../../lib/dataService";
import iconMail from '../../../assets/icon-mail.png'
import iconPhone from '../../../assets/icon-whatsapp.png'
import iconSMS from '../../../assets/icon-sms.png'

const useSortableData = (items, config = null) => {
  const [sortConfig, setSortConfig] = React.useState(config);

  const sortedItems = React.useMemo(() => {
    let sortableItems = items;
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = key => {
    let direction = 'ascending';
    if (sortConfig && sortConfig.key === key && sortConfig.direction === 'ascending') {
      direction = 'descending';
    }
    setSortConfig({ key, direction });
  }

  return { items: sortedItems, requestSort, sortConfig };
}

const BuyersTable = () => {
  const [buyers, setBuyers] = useState(null)

  const { items, requestSort, sortConfig } = useSortableData(buyers);
  const getClassNamesFor = (name) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };

  useEffect(() => {
    DataService.findAll()
        .then(res => {
          setBuyers(res.buyers)
        })
  }, [])

  return items ? (
      <table className="table">
        <thead className="table-head">
          <tr>
            <th>№</th>
            <th onClick={() => requestSort('nickname')}
                className={getClassNamesFor('nickname')}>
              <div className="row">Nickname <SortIcons/></div>
            </th>
            <th onClick={() => requestSort('country')}
                className={getClassNamesFor('country')}>
              <div className="row">Country <SortIcons/></div>
            </th>
            <th onClick={() => requestSort('weeklyDemand')}
                className={'no-mob ' + getClassNamesFor('weeklyDemand')}>
              <div className="row">Weekly Demand <SortIcons/></div>
            </th>
            <th onClick={() => requestSort('trucksSold')}
                className={'no-mob ' + getClassNamesFor('trucksSold')}>
              <div className="row">Number Of <br/> Trucks Sold <SortIcons/></div>
            </th>
            <th onClick={() => requestSort('lastSale')}
                className={'no-mob ' + getClassNamesFor('lastSale')}>
              <div className="row">Last Sale Date <SortIcons/></div>
            </th>
            <th onClick={() => requestSort('price')}
                className={'no-mob ' + getClassNamesFor('price')}>
              <div className="row">Price Per Ton <SortIcons/></div>
            </th>
            <th onClick={() => requestSort('profitPerTone')}
                className={getClassNamesFor('profitPerTone')}>
              <div className="row">Profit Per Ton<SortIcons/></div>
            </th>
            <th onClick={() => requestSort('rating')}
                className={getClassNamesFor('rating')}>
              <div className="row">Rating <SortIcons/></div>
            </th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        {items.map((item, i) => (
            <tr key={i}>
              <td className='table-first'>{i+1}</td>
              <td>{item.nickname}</td>
              <td>{item.country}</td>
              <td className={'no-mob'}>{item.weeklyDemand}</td>
              <td className={'no-mob'}>{item.trucksSold}</td>
              <td className={'no-mob'}>{item.lastSale ? item.lastSale : "No sales"}</td>
              <td className={'no-mob'}>{item.price} PLN</td>
              <td className={item.profitPerTone >= 0 ? 'green': 'red'}>{item.profitPerTone} PLN</td>
              <td><div className={ (item.rating > 4.5 && "rating high") || (item.rating < 3 && "rating low") || "rating"}>{item.rating}</div></td>
              <td className={'table-actions'}>
                <div className="row">
                  <a href="#" className={'table-icon'}>
                    <img src={iconMail} alt=""/>
                  </a>
                  <a href="#" className={'table-icon'}>
                    <img src={iconPhone} alt=""/>
                  </a>
                  <a href="#" className={'table-icon'}>
                    <img src={iconSMS} alt=""/>
                  </a>
                  <a href="#" className={'icon-more'}>
                    <span/>
                    <span/>
                    <span/>
                  </a>
                </div>

              </td>
            </tr>
        ))}

        </tbody>
      </table>
  ) : (<span/>)
}

export default BuyersTable