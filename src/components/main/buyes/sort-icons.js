import React from 'react'
import IconChevron from '../../../assets/icon-chevron-2.svg'

const SortIcons = ({sortType}) => {
  return (
    <div className="sort-icons">
      <div className={!sortType || sortType === 'asc' ? 'sort-icons-item up' : 'sort-icons-item'}>
        <IconChevron/>
      </div>
      <div className={!sortType || sortType === 'desc' ? 'sort-icons-item down' : 'sort-icons-item'}>
        <IconChevron/>
      </div>
    </div>
  )
}

export default SortIcons