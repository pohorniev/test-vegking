import React from 'react'
import Products from "./products/products";
import BuyersTable from "./buyes/buyers-table";
import iconMail from '../../assets/icon-mail.png'
import iconPhone from '../../assets/icon-whatsapp.png'

const Main = () => {
  return (
    <main className='main-content'>
      <h1 className="content-title">
        Chose stock
      </h1>
      <Products/>

      <h1 className="content-title">
        Buyers list
      </h1>
      <div className="buyers-top">
        <div className="buyers-top-link">Send offer for all buyers</div>
        <div className="buyers-top-panel">
          <a href="#" className="buyers-top-icon">
            <img src={iconMail} alt=""/>
          </a>
          <a href="#" className="buyers-top-icon">
            <img src={iconPhone} alt=""/>
          </a>
        </div>
      </div>
      <BuyersTable />
    </main>
  )
}

export default Main;