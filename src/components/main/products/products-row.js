import React,{useEffect,useRef} from 'react'
import Swiper from 'swiper'
import ProductsItem from "./products-item";
import IconChevron from "../../../assets/icon-down-chevron.svg";

const ProductsRow = ({products}) => {




  return products ? (
    <>
      <div className='products-row swiper-container' id='products-row'>
        <div className="swiper-wrapper">
          {products.map(item => (
              <ProductsItem key={item.id} product={item} />
          ))}
        </div>
      </div>

    </>
  ) : (<span/>)
}

export default ProductsRow