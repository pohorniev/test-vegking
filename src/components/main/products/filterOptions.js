export const optProducts = [
  { id: 'potatos', label: 'Potatos' },
  { id: 'apples', label: 'Apples' },
  { id: 'onions', label: 'Peeled Onions' },
  { id: 'tomato', label: 'Tomato' },
  { id: 'cauliflower', label: 'Cauliflower' }
]

export const optSeller = [
  { id: 'vegking', label: 'VegKing' },
  { id: 'vegking1', label: 'VegKing' }
]

export const optVeraity = [
  { id: 'solist', label: 'Solist' },
  { id: 'solist1', label: 'Solist' }
]

export const optColor = [
  { id: 'yellow', label: 'Yellow' },
  { id: 'green', label: 'Green' }
]

export const optQuality= [
  { id: 'unwashed', label: 'Unwashed/Unwashable' },
  { id: 'washed', label: 'Washed' }
]