import React, { useState, useEffect, useRef } from 'react'
import ProductsTop from "./products-top";
import ProductsRow from "./products-row";

import {DataService} from "../../../lib/dataService";
import Swiper from "swiper";

const Products = () => {
  const [products, setProducts] = useState(null)
  const swiper = useRef(null)

  useEffect(()=> {
    if (products && document.getElementById('products-row')) {
      let productsRow = document.getElementById('products-row');

      swiper.current = new Swiper(productsRow, {
        direction: 'horizontal',
        slidesPerView: 4,
        spaceBetween: 20,
        navigation: {
          nextEl: "#btn-prev",
          prevEl: "#btn-next",
        },
        observer: true,
        observeParents: true,
        breakpoints: {
          1600: {
            slidesPerView: 5
          },
          1200: {
            slidesPerView: 4
          },
          992: {
            slidesPerView: 3
          },
          500: {
            slidesPerView: 2
          },
          300: {
            slidesPerView: 1
          }
        }
      })
    }

  })

  useEffect(()=>{
    if (products) {
      swiper.current.update()
      console.log('update')
    }
  },[products])

  useEffect(() => {
    DataService.findAll()
        .then(res => {
          setProducts(res.products)
        })
  }, [])

  return (
      <div className='products'>
        <ProductsTop swiper={swiper} />
        <ProductsRow products={products}  />
      </div>
  )
}

export default Products