import React, {useState} from 'react'
import Autocomplete from "./autocomplete";
import {optProducts, optColor, optSeller, optVeraity, optQuality} from "./filterOptions";
import IconChevron from "../../../assets/icon-down-chevron.svg";

const ProductsTop = ({swiper}) => {
  const [filters, setFilters] = useState({
    products: '',
    seller: '',
    veraity: '',
    color: '',
    quality: ''
  })

  return (
      <div className="products-top">
        <div className="products-filter">
          <Autocomplete options={optProducts}
                        inputName={'products'}
                        filter={filters}
                        handleSubmit={setFilters}
                        placeholder={"Product"}/>
          <Autocomplete options={optSeller}
                        inputName={'seller'}
                        filter={filters}
                        handleSubmit={setFilters}
                        placeholder={"Seller"}/>
          <Autocomplete options={optVeraity}
                        inputName={'veraity'}
                        filter={filters}
                        handleSubmit={setFilters}
                        placeholder={"Veraity"}/>
          <Autocomplete options={optColor}
                        inputName={'color'}
                        filter={filters}
                        handleSubmit={setFilters}
                        placeholder={"Color"}/>
          <Autocomplete options={optQuality}
                        inputName={'quality'}
                        filter={filters}
                        handleSubmit={setFilters}
                        placeholder={"Quality Type"}/>
        </div>

        <div className="products-top-panel">
          <a href='#' className="products-top-panel__link add-link">Add new product</a>
          <a href='#' className="products-top-panel__link">See all</a>
          <div className="products-top-panel__nav prev" id='btn-prev' onClick={() => swiper.current.slidePrev()}>
            <IconChevron/>
          </div>
          <div className="products-top-panel__nav next" id='btn-next' onClick={() => swiper.current.slideNext()}>
            <IconChevron/>
          </div>
        </div>
      </div>
  )
}

export default ProductsTop