import React from 'react'

const ProductsItem = ({product}) => {
  return (
    <a href={product.url} className='products-card swiper-slide'>
      <div className="products-card-img">
        <img src={product.photo} alt={product.title}/>
      </div>
      <div className="products-card-content">
        <div className="products-card-row">
          Seller: {product.seller}
        </div>
        <div className="products-card-row">
          Veraity: {product.veraity}
        </div>
        <div className="products-card-row">
          Flesh Color: {product.fleshColor}
        </div>
        <div className="products-card-row">
          Market Processing: {product.marketProcessing}
        </div>
        <div className="products-card-row">
          QualityType: {product.qualityType}
        </div>
        <div className="products-card-row">
          Soil: {product.soil}
        </div>
        <div className="products-card-row">
          Size: {product.size}
        </div>
        <div className="products-card-row">
          Packing: {product.packing}
        </div>
        <div className="products-card-row">
          Loc: {product.loc}
        </div>
        <div className="products-card-price">
          {product.price} EUR/Tone
        </div>
      </div>
    </a>
  )
}

export default ProductsItem