import React, { useState, useEffect, useRef } from 'react';
import IconChevron from '../../../assets/icon-down-chevron.svg'

const Autocomplete = ({ value, inputName, placeholder, options, handleSubmit, filters }) => {
  const [keyword, setKeyword] = useState('');
  const [filteredList, setFilteredList] = useState(options);
  const [openList, setOpenList] = useState(false);

  const ref = useRef(null);

  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      setOpenList(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  });

  if (document.getElementById('filter-reset')) {
    document.getElementById('filter-reset').addEventListener("click",
        function() {
          setKeyword('');
        });
  }

  const handleChange = e => {
    let newArr = [];
    let value = e.target.value;
    options.forEach(function(item) {
      let indexWord = item.label.toLowerCase().indexOf(value.toLowerCase());

      if (item.label.substr(indexWord, value.length).toLowerCase() === value.toLowerCase()) {
        newArr.push({
          id: item.id,
          label: item.label,
        });
      }
    });

    setKeyword(value);
    setFilteredList(newArr);
    if (e.target.value.length < 1) {
      handleSubmit('');
    }
  };

  const handleOption = (result) => {
    setKeyword(result);
    handleSubmit({...filters, [inputName]: result});
    setOpenList(false);
  };

  return (
      <div className='products-filter-item autocomplete' ref={ref}>
        <span onClick={() => setOpenList(true)}>{keyword ? placeholder+': '+keyword : placeholder}</span>
        <IconChevron/>
        {openList && filteredList &&
        <div className="autocomplete-list">
          <input name={inputName}
                 autoComplete='off'
                 value={keyword}
                 onChange={handleChange}
                 placeholder={placeholder}/>
          {filteredList.map(item => (
              <div key={item.id} className="autocomplete-item" onClick={() => handleOption(item.label)}>
                {item.label}
              </div>
          ))}
        </div>
        }
      </div>
  );
};

export default Autocomplete;